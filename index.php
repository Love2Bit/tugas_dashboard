<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, true);

$global = $result['Global'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/icon.webp" type="image/x-icon">
    <title>Data Covid-19</title>
    <script src="js/Chart.js"></script>
    <script src="js/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
</head>
<body>
<style type="text/css">  
		#myChart{

			background-color: sandybrown;
      background-repeat: no-repeat;
      position: relative;
      width: 100%;
      height: 100%;
      background-size: 100%;
			font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
      }
    /* navbar */
	</style>
    <nav class="navbar navbar-expand-lg navbar-secondary bg-secondary navbar-fixed">
        <a class="navbar-brand" href="index.php"><img src="img/bumi.png" alt="logo" width="100px" height="100px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-link active" href="index.php" style="color: white; width: 80px; font-size: 20px;">Positive <span class="sr-only">(current)</span></a>
            <a class="nav-link" href="death.php" style="color: white; width: 80px; font-size: 20px; padding-left: 20px;">Death</a>
            <a class="nav-link" href="recovered.php" style="color: white; width: 80px; font-size: 20px; padding-left: 28px;">Recovered</a>
            <a class="nav-link" href="table.php" style="color: white; width: 80px; font-size: 20px; padding-left: 60px;">Table</a>
          </div>
       </nav>
    
   
<!-- card -->
<div class="container">
    <div class="row">
        <div class="col-12 s-12" id="bg">
            <div class="card-deck " style="margin-top: 40px;">
                <div class="card text-white bg-warning mb-3" style="box-shadow: 0 0 40px #333300">
                  <div class="card-body">
                    <h5 class="card-title"><i class="fas fa-viruses" style="font-size:40px; width:70px;"></i>Positive Covid-19</h5>
                    <p class="card-text"  style="font-size: 40px; color: white;"><?php echo number_format($global['TotalConfirmed']). " Soul"; ?></p>
                    
                  </div>
                </div>
                <div class="card text-white bg-danger mb-3">
                  <div class="card-body">
                    <h5 class="card-title"><i class="fas fa-skull-crossbones" style="font-size:40px; width:70px"></i>Total Deaths</h5>
                    <p class="card-text"  style="font-size: 40px; color: white;"><?php echo number_format($global['TotalDeaths']). " Soul"; ?></p>
                    
                  </div>
                </div>
                <div class="card text-white bg-success mb-3">
                  <div class="card-body">
                    <h5 class="card-title"><i class="fas fa-medkit" style="font-size:40px; width:70px"></i>Total Recovered</h5>
                    <p class="card-text"  style="font-size: 40px; color: white;"><?php  echo number_format($global['TotalRecovered']). " Soul"; ?></p>
                    
                  </div>
                </div>
              </div>


            <!-- Card -->
              <h1 style="margin-top:100px; text-align:center;">Total Confirmed Covid-19 Data in the World</h1>
              <div >
                <canvas id="myChart" width="500px" height="500px"></canvas>
            </div>
            
            
            <script>
                var ctx = document.getElementById("myChart").getContext('2d');
        
                var data=$.ajax({
                    url: 'https://api.covid19api.com/summary',
                    Cache:false   
                })
        
                .done(function (html){
                    function get_all_countries(html){
                        var temp_arr=[];
                        html.Countries.forEach(function (el){
                            temp_arr.push(el.Country);
                        })
                        return temp_arr;
                    }
        
                    function get_total(html){
                        var temp_total=[];
                        html.Countries.forEach(function (co){
                            temp_total.push(co.TotalConfirmed);
                        })
                        return temp_total;
                    }
        
                    var colors=[];
                     function color_random(){
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        }
        for (var i in html.Countries) {
            colors.push(color_random());
        }
                
                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: get_all_countries(html),
                        datasets: [{
        
                            label: '# of Votes',
                            data: get_total(html),
                            backgroundColor: colors,
                            borderColor: "rgba(0,0,0,2)",
                            borderWidth: 0
                        }]
                    },
                    options: {
                        responsive:true,
                        legend: {
                            position: 'left',
                        }
                    }
                    
                });
            });
            </script>
        
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        
        </body>
        </html>
        </div>
    </div>
</div>
      


            <!-- footer -->
            <section class="container-fluid">
            <div class="row mt-5 bg-dark">
              <p style="color: white;">Copyright &copy; Aodellas Magosiang 2020</p>
            </div>
          </section>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>