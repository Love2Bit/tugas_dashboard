<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, true);

$global = $result['Global'];
$countries = $result['Countries'];
foreach($countries as $key=>$value);
$i= 1;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/icon.webp" type="image/x-icon">
    <title>Data Covid-19</title>
    <script src="js/Chart.js"></script>
    <script src="js/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
</head>
<body>
    <style type="text/css">
        #tbody{
            background-image: url(img/bumi.jpg);
        }
    </style>

    <!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-secondary bg-secondary navbar-fixed">
        <a class="navbar-brand" href="index.php"><img src="img/bumi.png" alt="logo" width="100px" height="100px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-link active" href="index.php" style="color: white; width: 80px; font-size: 20px;">Positive <span class="sr-only">(current)</span></a>
            <a class="nav-link" href="death.php" style="color: white; width: 80px; font-size: 20px; padding-left: 20px;">Death</a>
            <a class="nav-link" href="recovered.php" style="color: white; width: 80px; font-size: 20px; padding-left: 28px;">Recovered</a>
            <a class="nav-link" href="table.php" style="color: white; width: 80px; font-size: 20px; padding-left: 60px;">Table</a>
          </div>
        </div>
      </nav>

      <!-- card -->
<div class="container">
    <div class="row">
        <div class="col-12 s-12" id="bg">
            <div class="card-deck " style="margin-top: 40px;">
                <div class="card text-white bg-warning mb-3">
                  <div class="card-body">
                    <h5 class="card-title"><i class="fas fa-viruses" style="font-size:40px; width:70px"></i>Positive Covid-19</h5>
                    <p class="card-text"  style="font-size: 40px;"><?php echo number_format($global['TotalConfirmed']). " Soul"; ?></p>
                    
                  </div>
                </div>
                <div class="card text-white bg-danger mb-3">
                  <div class="card-body">
                    <h5 class="card-title"><i class="fas fa-skull-crossbones" style="font-size:40px; width:70px"></i>Total Deaths</h5>
                    <p class="card-text"  style="font-size: 40px;"><?php echo number_format($global['TotalDeaths']). " Soul"; ?></p>
                    
                  </div>
                </div>
                <div class="card text-white bg-success mb-3">
                  <div class="card-body">
                    <h5 class="card-title"><i class="fas fa-medkit" style="font-size:40px; width:70px"></i>Total Recovered</h5>
                    <p class="card-text"  style="font-size: 40px;"><?php  echo number_format($global['TotalRecovered']). " Soul"; ?></p>
                    
                  </div>
                </div>
              </div>


              <h1 style="text-align: center; margin-top:100px;">World Wide Covid-19 Data Table</h1>
            <!-- Table -->
            <table class="table table-dark">
  <thead class="bg-primary">
    <tr>
      <th scope="col">Number</th>
      <th scope="col">Country</th>
      <th scope="col">Positive</th>
      <th scope="col">Death</th>
      <th scope="col">Recovered</th>
    </tr>
  </thead>
  <?php  foreach($countries as $key): ?>
  <tbody >
    <tr>
      <th><?php echo $i++; ?></th>
      <td><?php echo $key['Country']; ?></td>
      <td><?php  echo number_format($key['TotalConfirmed']); ?></td>
      <td><?php  echo number_format($key['TotalDeaths']); ?></td>
      <td><?php  echo number_format($key['TotalRecovered']); ?></td>
    </tr>
  </tbody>
  <?php  endforeach; ?>
</table>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
